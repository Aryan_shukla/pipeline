package com.wipro.scientificcalculator;

public class Trigcalc {
	public double getSinValue(int degree) {
		double radians= Math.toRadians(degree);	
		double sinValue=Math.sin(radians);
		return sinValue;
		}
}

